# License: GNU Affero General Public License v3 or later
# A copy of GNU AGPL v3 should have been included in this software package in LICENSE.txt.

from __future__ import print_function, division

from antismash import utils
from antismash.specific_modules.nrpspks.Minowa.base import run_minowa

def run_minowa_cal(infile, outfile):
    run_minowa(query_file = infile,
               startpos = 43,
               outfile = outfile,
               muscle_ref = utils.get_full_path(__file__, "CAL_domains_muscle.fasta"),
               ref_sequence = "Q54297_CAL1",
               positions_file = utils.get_full_path(__file__, "CALpositions.txt"),
               data_dir = utils.get_full_path(__file__, 'CAL_HMMs'),
               hmm_names = ["Acetyl-CoA",
                            "AHBA",
                            "fatty_acid",
                            "NH2",
                            "shikimic_acid"])

