## Author: Marnix Medema
## University of Groningen
## Department of Microbial Physiology / Groningen Bioinformatics Centre

from antismash import utils

def run_pkssignature_analysis(infile2, outfile):
    ##Core script
    #Extract PKS signature from AT domains
    infile = utils.get_full_path(__file__, "AT_domains_muscle.fasta")
    muscle_file = "muscle.fasta"
    dict2 = utils.read_fasta(infile2)
    namesb = dict2.keys()
    seqsb = dict2.values()
    startpos = 7
    querysignames = []
    querysigseqs = []
    for i in namesb:
      seq = seqsb[namesb.index(i)]
      querysignames.append(i)
      utils.writefasta([i],[seq],"infile.fasta")
      infile2 = "infile.fasta"
      refsequence = "P0AAI9_AT1"
      namesa = [i]
      #Run muscle and collect sequence positions from file
      utils.execute(["muscle", "-profile", "-quiet", "-in1", infile, "-in2", infile2, "-out", "muscle.fasta"])
      file = open(utils.get_full_path(__file__, "ATpositions.txt"), "r")
      text = file.read()
      text = text.strip()
      text = text.replace(' ','_')
      positions = text.split("\t")
      positions2 = []
      for i in positions:
        pos = int(i)
        pos = pos - startpos
        positions2.append(pos)
      positions = positions2
      #Count residues in ref sequence and put positions in list
      muscle_dict = utils.read_fasta(muscle_file)
      refseq = muscle_dict[refsequence]
      poslist = []
      a = 0
      b = 0
      c = 0
      while refseq != "":
        i = refseq[0]
        if c in positions and i != "-":
          poslist.append(b)
        if i != "-":
          c += 1
        b += 1
        refseq = refseq[1:]
      #Extract positions from query sequence
      query = namesa[0]
      query_seq = muscle_dict[query]
      seq = ""
      for j in poslist:
        aa = query_seq[j]
        seq = seq + aa
      querysigseqs.append(seq)

    #Load reference PKS signatures
    infile3 = utils.get_full_path(__file__, "pks_signatures.fasta")
    signaturesdict = utils.read_fasta(infile3)
    signaturenames = signaturesdict.keys()
    signatureseqs = signaturesdict.values()

    out_file = open(outfile,"w")
    #Compare PKS signature with database of signatures and write output to txt file
    for k in querysignames:
      querysigseq = querysigseqs[querysignames.index(k)]
      scoredict = {}
      for i in signaturenames:
        sigseq = signatureseqs[signaturenames.index(i)]
        positions  = range(len(querysigseq))
        score = 0
        for j in positions:
          if querysigseq[j] == sigseq[j]:
            score += 1
        score = ((float(score) / 24) * 100)
        scoredict[i] = score
      sortedhits = utils.sortdictkeysbyvaluesrev(scoredict)
      sortedhits = sortedhits[:10]
      sortedscores = []
      sortedhits2 = []
      for i in sortedhits:
        score = scoredict[i]
        if score > 50:
          score = "%.0f"%(score)
          sortedscores.append(score)
          sortedhits2.append(i)
      sortedhits = sortedhits2
      #Write output to txt file
      out_file.write("//\n" + k + "\t" + querysigseq + "\n")
      a = 0
      for i in sortedhits:
        out_file.write(i + "\t" + signatureseqs[signaturenames.index(i)] + "\t" + sortedscores[a] + "\n")
        a += 1
      out_file.write("\n\n")
